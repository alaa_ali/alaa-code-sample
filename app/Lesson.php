<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = [
        'course_id', 'lesson_no', 'title', 'descreption'
    ];

    public function Course()
    {
      return $this->belongsTo('app\Course');
    }

    public static function createLesson(Array $attributes = [])
    {
        $model = (new static)->create([
            'course_id' => $attributes['course_id'],
            'lesson_no' => $attributes['lesson_no'],
            'title' => $attributes['title'],
            'descreption' => $attributes['descreption']
        ]);
        $model->save();
        return $model;
    }
    
    public function updateLesson(Array $attributes = [])
    {
        $model -> update([
            'course_id' => $attributes['course_id'],
            'lesson_no' => $attributes['lesson_no'],
            'title' => $attributes['title'],
            'descreption' => $attributes['descreption']
        ]);
        return $model;
    }
    
}
