<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'title', 'trainer', 'start_date', 'descreption'
    ];

    public function Lessons()
    {
      return $this->hasMany('app\Lesson');
    }

    public static function createCourse(Array $attributes = [])
    {
        $model = (new static)->create([
            'title' => $attributes['title'],
            'trainer' => $attributes['trainer'],
            'start_date' => $attributes['start_date'],
            'descreption' => $attributes['descreption']
        ]);
        $model->save();
        return $model;
    }
    
    public function updateCourse(Array $attributes = [])
    {
        $model -> update([
            'title' => $attributes['title'],
            'trainer' => $attributes['trainer'],
            'start_date' => $attributes['start_date'],
            'descreption' => $attributes['descreption']
        ]);
        return $model;
    }

}
